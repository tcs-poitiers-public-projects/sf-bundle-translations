<?php

namespace TCS\TranslationBundle\Manager;

/**
 * Locale manager interface.
 */
interface LocaleManagerInterface
{
    /**
     * @return array
     */
    public function getLocales();
}
