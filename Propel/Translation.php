<?php

namespace TCS\TranslationBundle\Propel;

use TCS\TranslationBundle\Propel\Base\Translation as BaseTranslation;
use TCS\TranslationBundle\Manager\TranslationInterface;

class Translation extends BaseTranslation implements TranslationInterface
{
}
