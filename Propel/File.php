<?php

namespace TCS\TranslationBundle\Propel;

use TCS\TranslationBundle\Propel\Base\File as BaseFile;
use TCS\TranslationBundle\Manager\FileInterface;

class File extends BaseFile implements FileInterface
{
    /**
     * Set file name
     *
     * @param string $name
     */
    public function setName($name)
    {
        [$domain, $locale, $extention] = explode('.', $name);

        $this
            ->setDomain($domain)
            ->setLocale($locale)
            ->setExtention($extention)
        ;

        return $this;
    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getName()
    {
        return sprintf('%s.%s.%s', $this->getDomain(), $this->getLocale(), $this->getExtention());
    }
}
