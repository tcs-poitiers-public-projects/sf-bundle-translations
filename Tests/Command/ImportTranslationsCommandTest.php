<?php

namespace TCS\TranslationBundle\Tests\Command;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\CreateSchemaDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DropSchemaDoctrineCommand;
use TCS\TranslationBundle\Manager\LocaleManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Tester\CommandTester;
use TCS\TranslationBundle\Command\ImportTranslationsCommand;

/**
 * Test the translations import command, with option and arguments
 *
 * @covers \TCS\TranslationBundle\Command\ImportTranslationsCommand
 */
class ImportTranslationsCommandTest extends WebTestCase
{
    private static Application $application;

    /**
     *
     */
    public function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        static::$application = new Application(static::$kernel);

        static::addDoctrineCommands();

        static::rebuildDatabase();
    }

    /**
     *
     */
    private static function addDoctrineCommands()
    {
        static::$application->add(new DropSchemaDoctrineCommand());
        static::$application->add(new CreateSchemaDoctrineCommand());
    }

    /**
     *
     */
    private static function rebuildDatabase()
    {
        static::$kernel->getContainer()->get('doctrine.dbal.default_connection');

        static::runCommand("doctrine:schema:drop", ['--force' => true]);
        static::runCommand("doctrine:schema:create");
    }

    private static function runCommand($commandName, $options = [])
    {
        $options["-e"] = self::$kernel->getEnvironment();

        $options['command'] = $commandName;

        $input = new ArrayInput($options);
        $output = new NullOutput();

        static::$application->setAutoExit(false);
        self::$application->run($input, $output);
    }

    /**
     * Test execute with all the options
     *
     * @group command
     */
    public function testExecute()
    {
        static::$application->add(
            new ImportTranslationsCommand(
                self::$kernel->getContainer()->get('translator'),
                self::$kernel->getContainer()->get(LocaleManagerInterface::class),
                self::$kernel->getContainer()->get('tcs_translation.importer.file')
            )
        );

        $command = static::$application->find("tcs:translations:import");

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'       => $command->getName(),
                'bundle'        => 'TCSTranslationBundle',
                '--cache-clear' => true,
                '--force'       => true,
                '--locales'     => ['en', 'fr'],
            ]
        );

        $resultLines = explode("\n", $commandTester->getDisplay());

        $this->assertEquals('# TCSTranslationBundle:', $resultLines[0]);
        $this->assertMatchesRegularExpression('/Using dir (.)+\/Resources\/translations to lookup translation files/', $resultLines[1]);
        $this->assertMatchesRegularExpression('/translations\/TCSTranslationBundle\.((fr)|(en))\.yml" \.\.\. 30 translations/', $resultLines[2]);
        $this->assertMatchesRegularExpression('/translations\/TCSTranslationBundle\.((fr)|(en))\.yml" \.\.\. 30 translations/', $resultLines[3]);
        $this->assertEquals('Removing translations cache files ...', $resultLines[4]);
    }
}
