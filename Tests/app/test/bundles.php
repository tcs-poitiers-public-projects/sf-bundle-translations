<?php

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use TCS\TranslationBundle\TCSTranslationBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;

return [
    new FrameworkBundle(),
    new DoctrineBundle(),
    new TCSTranslationBundle(),
];
