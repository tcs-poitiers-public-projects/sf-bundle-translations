<?php

namespace TCS\TranslationBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use TCS\TranslationBundle\Model\File as FileModel;
use TCS\TranslationBundle\Manager\FileInterface;

/**
 * @author Cédric Girard <c.girard@lexik.fr>
 */
#[UniqueEntity(fields: ['hash'])]
class File extends FileModel implements FileInterface
{
    /**
     * {@inheritdoc}
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime("now");
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }
}
