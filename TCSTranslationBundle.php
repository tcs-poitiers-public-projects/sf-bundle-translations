<?php

namespace TCS\TranslationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use TCS\TranslationBundle\DependencyInjection\Compiler\RegisterMappingPass;
use TCS\TranslationBundle\DependencyInjection\Compiler\TranslatorPass;

/**
 * Bundle main class.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class TCSTranslationBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TranslatorPass());
        $container->addCompilerPass(new RegisterMappingPass());
    }
}
